import React, { Component } from 'react';

import './style.scss';
import Header from './components/layout/Header';
import Content from './components/layout/Content';
import Footer from './components/layout/Footer';

class App extends Component {
   
   constructor(props){
   	super(props);

   	this.state = {
   		loading: true,
   		opacity: 1
   	}
   }

   componentDidMount(){
   	// setTimeout( () => {
   	// 	this.setState({opacity: 0});
   	// }, 3000); style={{opacity: `${this.state.opacity}` , transition: 'all 0.5s ease'}}
   	// 3000 
   	setTimeout( () => {
   		this.setState({loading: false});
   	}, 3500);
   	// 3500
   }

   render() {
      const { loading } = this.state

   	if(loading)
   	{
   		return (
   			<div className="loading-screen">
					<div className="loader-image"></div>
   			</div>
			);
   	}

      return (
         <div>
            <Header />
            <Content />
            <Footer />
         </div>
      );
   }
}

export default App;