import React, { Component } from 'react';

import { Link } from 'react-router-dom';
import FaEnvelope from 'react-icons/lib/fa/envelope';
import FaFax from 'react-icons/lib/fa/fax';
import FaPhone from 'react-icons/lib/fa/phone';

class Footer extends Component {

   render() {
      return (
         <div className="container-fluid footer-container">
            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="footer-top">
                     <img className="company-logo" src={require('../../images/logo.svg')} alt="Company Logo" />
                  </div>
                  <div className="footer-center">
                     <Link className="nav-link" to='/'>home</Link>
                     <Link className="nav-link" to='/products'>produk</Link>
                     <Link className="nav-link" to='/order'>pemesanan</Link>
                     <Link className="nav-link" to='/news'>berita</Link>
                     <Link className="nav-link" to='/about'>tentang kami</Link>
                     <Link className="nav-link" to='/contact'>hubungi kami</Link>
                  </div>
                  <div className="footer-bottom">
                     <div className="row no-gutters footer-contact">
                        <div className="col-12 col-sm-12 col-md-12">
                           <p>
                              itc mangga dua
                              <br/>
                              lt. dasar
                              <br/>
                              blok : e 1, no : 50 - 51
                              <br/>
                              jln. mangga dua raya
                              <br/>
                              jakarta utara
                              <br/>
                              dki jakarta
                              <br/>
                              <FaEnvelope />&nbsp;<span>email&nbsp;:&nbsp;</span>atk.pelangimandiri@gmail.com
                              <br/>
                              <FaPhone />&nbsp;<span>phone&nbsp;:&nbsp;</span>021-6014555&emsp;
                              <FaFax />&nbsp;<span>fax&nbsp;:&nbsp;</span>021-6014554
                           </p>   
                        </div>
                     </div>
                     <div className="footer-copyright">   
                        <span className="company-title">pelangi&nbsp;mandiri</span>
                        <p>copyright &copy; 2017.&nbsp;&nbsp;all rights reserved</p>
                     </div>   
                  </div>
               </div>
            </div>
         </div>
      );
   }
}

export default Footer;