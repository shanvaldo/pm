import React, { Component } from 'react';

import { Link } from 'react-router-dom';

class Header extends Component {

   constructor(props) {
      super(props);

      this.toggle = this.toggle.bind(this);
      this.state = {
         isOpen: false,
         activeMenuName: ''
      };
   }

   toggle() {
      this.setState({
         isOpen: !this.state.isOpen
      });
   }

   render() {

      return (
         <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-faded">
               <Link className="navbar-brand" to="/">
                  <img className="company-logo" src={require('../../images/logo.svg')} alt="Company Logo" />
                  <span className="company-title">pelangi&nbsp;mandiri</span>
               </Link>
               <button className={this.state.isOpen ? 'hamburger hamburger--stand is-active' : 'hamburger hamburger--stand'} type="button" onClick={this.toggle}>
                  <span className="hamburger-box">
                     <span className="hamburger-inner"></span>
                  </span>
               </button>
               <div className={this.state.isOpen ? 'collapse navbar-collapse custom-navbar show' : 'collapse navbar-collapse custom-navbar hide'}>
                  <ul className="navbar-nav mr-auto">
                     <li className="nav-item">
                        <Link className="nav-link custom-hover" to="/" onClick={this.toggle}>home</Link>
                     </li>
                     <li className="nav-item">
                        <Link className="nav-link custom-hover" to="/products" onClick={this.toggle}>produk</Link>
                     </li>
                     <li className="nav-item">
                        <Link className="nav-link custom-hover" to="/order" onClick={this.toggle}>pemesanan</Link>
                     </li>
                     <li className="nav-item">
                        <Link className="nav-link custom-hover" to="/about" onClick={this.toggle}>tentang kami</Link>
                     </li>
                     <li className="nav-item">
                       <Link className="nav-link custom-hover" to="/news" onClick={this.toggle}>berita</Link>
                     </li>
                     <li className="nav-item">
                        <Link className="nav-link custom-hover" to="/contact" onClick={this.toggle}>hubungi kami</Link>
                     </li>
                  </ul>
               </div>
            </nav>
         </div>
      );
   }
}

export default Header;