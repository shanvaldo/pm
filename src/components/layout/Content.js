import React, { Component } from 'react';

import {Route , Switch} from 'react-router-dom';
import Home from '../content/Home';
import Products from '../content/Products';
import ProductDetail from '../content/ProductDetail';
import Order from '../content/Order';
import AboutUs from '../content/AboutUs';
import News from '../content/News';
import NewsDetail from '../content/NewsDetail';
import Contact from '../content/Contact';

class Content extends Component {
   render() {
      return (
	    	<Switch>
	  			<Route exact path='/' component={Home}/>
	  			<Route exact path='/products' component={Products}/>
	  			<Route exact path='/order' component={Order}/>
	  			<Route exact path='/products/:brand' component={ProductDetail}/>
	  			<Route exact path='/about' component={AboutUs}/>
	  			<Route exact path='/news' component={News}/>
	  			<Route exact path='/news/:news_detail' component={NewsDetail}/>
	  			<Route exact path='/contact' component={Contact}/>
			</Switch>
      );
   }
}

export default Content;