import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';

class Products extends Component {

   constructor(props){
      super(props);

      this.state = {
         product_brands: [],
         loading: true
      };
   }

   componentWillMount(){
      window.scrollTo(0, 0);
      const api = 'https://api.atk-pelangimandiri.com/api/products';

      axios.get(api)
         .then(res => {
            // console.log(res);

            this.setState({
               product_brands: res.data,
            })

            setTimeout( () => {
               this.setState({loading: false});
            }, 1500);
         })
         .catch(err => {
            console.log(err)
         })
   }

   getBrand() {

      const brands = this.state.product_brands.map((brand) =>
         <div key={brand.id} className="col-6 col-sm-6 col-md-4 product-box-container">
            <Link to={"/products/" + brand.id}>
               <div className="product-box">
                  <h1>{brand.name}</h1>
                  {
                     brand.image === null ? <img src={'https://ak9.picdn.net/shutterstock/videos/21522739/thumb/1.jpg'} alt={brand.name}/> :<img src={'https://api.atk-pelangimandiri.com/api/image/'+brand.id} alt={brand.name}/>
                  }
               </div>
            </Link>   
         </div>
      );

      return brands
   }

   render() {
      const { loading } = this.state

      if(loading)
      {
         return (
            <div className="loading-screen2">
               <Helmet>
                  <title>Produk</title>
               </Helmet>
               
               <div className="dizzy-gillespie"></div>
               <h1>loading...</h1>
            </div>
         );
      }

      return (
         <div className="container-fluid products-container">
            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12 products-title">
                  <h1>produk kami</h1>
               </div>    
               <div className="col-12 col-sm-12 col-md-12 text-effect-wrapper">
                  <div className="overlay"></div>
                  <div className="text-effect">
                     <div className="first-text-effect">
                        <span>best stationary</span> 
                     </div>    
                     <div className="last-text-effect"> 
                        <span>for the amazing experience</span>
                     </div>
                  </div>
               </div>    
            </div>

            <div className="row no-gutters products-list-title">
               <div className="col-12 col-sm-12 col-md-12">
                  <h1>brands</h1>
               </div>
            </div>
            
            <div className="row no-gutters products">
               {this.getBrand()}
            </div>

         </div>
      );
   }
}

export default Products;