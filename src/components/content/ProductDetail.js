import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import TiArrowRight from 'react-icons/lib/ti/arrow-right';

class ProductDetail extends Component {

   constructor(props){
      super(props);

      this.state = {
         brand_id : this.props.match.params.brand,
         brand_name : '',
         product_original: [],
         product: [],
         selected_filter_option: 'none',
         loading: true
      };

      this.getSelectedFilterOption = this.getSelectedFilterOption.bind(this);
      this.handleChange = this.handleChange.bind(this);
   }

   componentWillMount(){
      window.scrollTo(0, 0);
      const {brand_id} = this.state;
      const api = 'https://api.atk-pelangimandiri.com/api/products/'+brand_id;

      axios.get(api)
         .then(res => {
            // console.log(res.data);

            this.setState({
               brand_name: res.data.brand_name.name,
               product_original: res.data.list_of_products,
               product: res.data.list_of_products
            });

            setTimeout( () => {
               this.setState({loading: false});
            }, 1500);
         })
         .catch(err => {
            console.log(err);
         })
   }

   getSelectedFilterOption(e){
      this.setState({ selected_filter_option: e.target.value }, () => {
         if(this.state.selected_filter_option === 'none')
         {
            this.setState({ product: this.state.product_original });
         }
      })
   }

   renderFilterData(){

      if(this.state.selected_filter_option !== 'none')
      {
         if(this.state.selected_filter_option === 'category')
         {
            const category = this.state.product_original.map((category) => {               
               if(category.product.length === 0)
               {
                  return '';
               }
               else
               {
                  return <option key={category.id} value={category.id}>{category.name}</option>
               }
            });

            return category
         }
      }
   }

   handleChange(e){
      const filter_by = this.state.selected_filter_option;
      const selected_filter_data = e.target.value;

      if(selected_filter_data !== 'default')
      {
         const api = 'https://api.atk-pelangimandiri.com/api/products/filter';
         axios.post(api,{
            brand_id: this.state.brand_id,
            filter_by: filter_by,
            selected_filter_data: selected_filter_data
         })
         .then(res => {
            // console.log(res.data);
            this.setState({
               product: res.data.list_of_products
            })
         })
         .catch(err => {
            console.log(err);
         })
      }   
   }


   renderProducts() {
      const product = this.state.product.map((category) => {
         
         if(category.product.length === 0)
         {
            return '';
         }
         else
         {
            return(
               <div key={category.id} className="col-12 col-sm-12 col-md-6 show-products">
                  <div className="card border-primary mb-3">
                     <div className="card-header">
                        <h1 className="card-title">{category.name}</h1>
                     </div>
                     <div  className="card-body text-primary">
                        <table className="table">
                           <tbody>
                              {   
                                 category.product.map((product) => {
                                    return(
                                       <tr key={product.id}>
                                          <td>{product.name}</td>
                                       </tr>
                                    );   
                                 })   
                              }
                           </tbody>
                        </table>  
                     </div>
                  </div>
               </div>
            );
         }   
      });

      return product
   }

   render() {
   
      const { loading } = this.state

      if(loading)
      {
         return (
            <div className="loading-screen2">
               <Helmet>
                  <title>{"Produk - "+this.state.brand_name}</title>
               </Helmet>

               <div className="dizzy-gillespie"></div>
               <h1>loading</h1>
            </div>
         );
      }

      return (

         <div className="container-fluid product-detail-container">
            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12 product-detail-title">
                  <h1>{this.state.brand_name}</h1>
                  <hr className="custom-line"/>
               </div>
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="form-group option-group">
                     <h1>Filter</h1>
                     <div className="form-check">
                        <label className="form-check-label">
                           <input type="radio" className="form-check-input" name="option" id="option_none" value="none" defaultChecked={true} onChange={this.getSelectedFilterOption}/>
                           tampilkan semua produk
                        </label>
                     </div>
                     <div className="form-check">
                        <label className="form-check-label">
                           <input type="radio" className="form-check-input" name="option" id="option_category" value="category" onChange={this.getSelectedFilterOption}/>
                           secara kategori produk
                        </label>
                     </div>
                  </div>
                  
                  {
                     this.state.selected_filter_option === 'category' ? 
                     <div className="form-group filter-products">
                        <label htmlFor="product-brand">filter by {this.state.selected_filter_option}</label>
                        <select id="product-brand" className="form-control" onChange={this.handleChange}>
                           <option value="default">select {this.state.selected_filter_option}</option>
                           {this.renderFilterData()}
                        </select>
                     </div>
                     : ''
                  }

                  <h1 className="show-products-title">daftar produk</h1>
               </div>
               {this.renderProducts()}
               <div className="col-12 col-sm-12 col-md-12 order-product-highlight">
                  <hr className="custom-line"/>
                  <p>Jika anda ingin melakukan pemesanan produk silahkan klik tombol berikut ini</p>
                  <Link to='/order'>   
                     <button className="btn btn-primary btn-lg">pesan produk sekarang<TiArrowRight/></button>
                  </Link>
               </div>
            </div>  
         </div>
      );
   }
}

export default ProductDetail;