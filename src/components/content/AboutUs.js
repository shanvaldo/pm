import React, { Component } from 'react';

import {Helmet} from "react-helmet";

class AboutUs extends Component {

   componentWillMount(){
      window.scrollTo(0, 0);
   }

   render() {
   
      return (
         <div className="container-fluid about-container">
            <Helmet>
                <title>Tentang Kami</title>
            </Helmet>

            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12 about-title">
                  <h1>Tentang Kami</h1>
               </div>
               <div className="about-background"></div>
               <div className="about-content">
                  <p>Pelangi Mandiri merupakan salah satu penyedia alat tulis kantor terlengkap dan terpercaya.
                  Dengan penyediaan produk yang terlengkap serta dengan harga yang bersaing tentunya akan selalu membuat anda sebagai
                  konsumen merasa nyaman dan puas terhadap kinerja kami.</p>
               </div>
            </div>
         </div>
      );
   }
}

export default AboutUs;