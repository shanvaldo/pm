import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import AlertContainer from 'react-alert';
import ReCAPTCHA from 'react-google-recaptcha';

class Order extends Component {

   constructor(props) {
      super(props);
      this.state = {
         email: '',
         name: '',
         subject: '',
         message: '',
         isError: false,
         errorMessage: [],
         reCaptcha: false
      };

      this.handleValue = this.handleValue.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.reCaptcha = this.reCaptcha.bind(this);
   }

   componentWillMount(){
      window.scrollTo(0, 0);
   }

   handleValue(e) {
      this.setState( {[e.target.name]: e.target.value} );
   }

   handleSubmit(e) {
      e.preventDefault();

      if(this.state.reCaptcha === true)
      {
         axios.post('https://api.atk-pelangimandiri.com/api/order',{
            name: this.state.name,
            email: this.state.email,
            subject: this.state.subject,
            message: this.state.message
         })
         .then(response => {
            this.setState({
               email: '',
               name: '',
               subject: '',
               message: '',
               isError: false,
               errorMessage: []
            })
            
            //Call React Alert
            this.showAlert();
         })
         .catch(error => {
            const status = error.response.status;

            if(status === 400)
            {   
               this.setState({
                  isError: true,
                  errorMessage: error.response.data.error_message
               })
            }   
         })
      }   
      else
      {
         this.reCaptchaAlert();
      }
   }

   //React Alert
   alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'dark',
      transition: 'scale'
   }

   showAlert = () => {
      this.msg.show('Terima kasih atas pemesanan anda!', {
         time: 10000,
         type: 'success',
      })
      window.grecaptcha.reset();
   }

   //Google ReCaptcha
   reCaptchaAlert = () => {
      this.msg.show('Harap memverifikasi ReCaptcha terlebih dahulu untuk melakukan pemesanan!', {
         time: 10000,
         type: 'error',
      })
   }

   reCaptcha(value) {
      this.setState({
         reCaptcha: true
      })
   }

   render() {
      
      const { name, email, subject, message, errorMessage } = this.state;

      return (
         <div className="container-fluid order-container">
            <Helmet>
                <title>Pemesanan</title>
            </Helmet>

            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12 order-content">
                  <h1>order product</h1>
                  
                  <form onSubmit={this.handleSubmit}>
                     <div className="form-group">
                        <label htmlFor="email_support">email</label>
                        <input className="form-control" type="email" name="email" value={email} placeholder="Email Anda" autoComplete="off" onChange={this.handleValue}/>
                        <span className="error-message">{errorMessage.email}</span>


                        <label htmlFor="name_support">nama</label>
                        <input className="form-control" type="text" name="name" value={name} placeholder="Nama anda atau nama perusahaan" autoComplete="off" onChange={this.handleValue}/>
                        <span className="error-message">{errorMessage.name}</span>

                        <label htmlFor="subject_support">subjek</label>
                        <input className="form-control" type="text" name="subject" value={subject} placeholder="e.g 'Pesanan saya'" autoComplete="off" onChange={this.handleValue}/>
                        <span className="error-message">{errorMessage.subject}</span>

                        <label htmlFor="message_support">pesanan anda</label>
                        <textarea className="form-control" name="message" value={message} rows="5" placeholder="Pemesanan produk yang anda inginkan kepada Pelangi Mandiri" onChange={this.handleValue}></textarea>
                        <span className="error-message">{errorMessage.message}</span>
                        <div className="captcha-container">
                           <ReCAPTCHA
                              ref="recaptcha"
                              sitekey="6LevzT0UAAAAAH2Qk-QR2ykPeIa2cSRkyjXf3wgw"
                              onChange={this.reCaptcha}
                           />
                        </div>  
                        <button type="submit" className="btn btn-primary btn-lg">pesan sekarang</button> 
                        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />                    
                     </div>
                  </form>  
               </div>
            </div>
         </div>
      );
   }
}

export default Order;