import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import Slider from 'react-slick';
import TiArrowRight from 'react-icons/lib/ti/arrow-right';

class Home extends Component {

	constructor(props) {
      super(props);
      this.state = {
         important_news: [],
      };
   }

	componentWillMount(){
      window.scrollTo(0, 0);

      const api = 'https://api.atk-pelangimandiri.com/api/news/important';

      axios.get(api)
         .then(res => {

            this.setState({
               important_news: res.data,
            })

            // console.log(res.data);
         })
         .catch(err => {
            console.log(err)
         })
   }

   getImportantNews(){
   	var image_counter = 0;
   	const rendered_news = this.state.important_news.map((news) =>{
			
			image_counter++;
			return(
				<div key={news.id} className="col-12 col-sm-12 col-md-6 home-news">
			      <div className="card">
			        	<div className="card-image">
			        		<img src={require('../../images/news-'+image_counter+'.png')} alt="News" />
			        	</div>	
			        	<div className="card-body">
			        		<div className="card-title">{news.title}</div>
			         	<div className="card-text">{news.message.substring(0,50)+'...'}</div>
			         	<Link className="btn btn-primary btn-lg news-button" to={"/news/" + news.id}>baca lebih lanjut<TiArrowRight /></Link>
			        	</div>
			      </div>
		   	</div>
	   	);
   	});

   	return rendered_news;
   }

  	render() {
  		var settings = {
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			arrows: true
      };

    	return (
			<div className="container-fluid home-container">
				<Helmet>
                <title>Perlengkapan Alat Tulis Kantor | Pelangi Mandiri</title>
            </Helmet>

				<div className="row no-gutters">
					<div className="col-12 col-sm-12 col-md-12 highlight">
						<div className="highlight">
				         <div className="overlay">
				          	<h1>Alat Tulis Kantor<br/>Pelangi Mandiri</h1>
				         </div>
				      </div>
					</div>
				</div>

				<div className="row no-gutters featured-products">
					<div className="col-12 col-sm-12 col-md-12">
						<h1>featured products</h1>
					</div>
					<div className="col-12 col-sm-12 col-md-12 slider-container">
						<div className="slider">
							<Slider {...settings}>
								<div className="slider-content">
						   		<img className="slider-image" src={require('../../images/featured_product_1.png')} alt="featured-product-1" />
								</div>
								<div className="slider-content">
						   		<img className="slider-image" src={require('../../images/featured_product_2.png')} alt="featured-product-2" />
								</div>
								<div className="slider-content">
						   		<img className="slider-image" src={require('../../images/featured_product_3.png')} alt="featured-product-3" />
								</div>
								<div className="slider-content">
						   		<img className="slider-image" src={require('../../images/featured_product_4.png')} alt="featured-product-4" />
								</div>
							</Slider>
						</div>
					</div>
					<div className="col-12 col-sm-12 col-md-12 featured-products-description-container">
						<div className="jumbotron featured-products-description">
					   	<Link className="btn btn-primary btn-lg explore-button" to={"/products"}>lihat semua produk<TiArrowRight /></Link>
						</div>
					</div>
				</div>

				<div className="row no-gutters featured-brands">
					<div className="col-12 col-sm-12 col-md-12">
			   		<h1>featured brands</h1>
			   	</div>
			   	<div className="col-6 col-sm-6 col-md-6 brand-left">
			   		<div className="brand-image">
			   			<img src={require('../../images/joyko.png')} alt="Joyko" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/bantex.png')} alt="Bantex" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/pilot.png')} alt="Pilot" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/panfix.png')} alt="Panfix" />
			   		</div>
			   	</div>
			   	<div className="col-6 col-sm-6 col-md-6 brand-right">
			   		<div className="brand-image">
			   			<img src={require('../../images/citizen.png')} alt="Citizen" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/snowman.png')} alt="Snowman" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/sinardunia.png')} alt="Sinardunia" />
			   		</div>
			   		<div className="brand-image">
			   			<img src={require('../../images/max.png')} alt="Max" />
			   		</div>
			   	</div>
				</div>
				<div className="row no-gutters featured-news">
			   	<div className="col-12 col-sm-12 col-md-12">
			   		<h1>news</h1>
			   	</div>
			   	{this.getImportantNews()}
			   </div>
			</div>
    	);
  	}
}

export default Home;