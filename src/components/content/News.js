import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import AlertContainer from 'react-alert'
import TiArrowRight from 'react-icons/lib/ti/arrow-right';

class News extends Component {

   constructor(props) {
      super(props);
      this.state = {
         email: '',
         isError: false,
         errorMessage: [],
         bunch_of_news: '',
         loading: true
      };

      this.handleValue = this.handleValue.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
   }

   componentWillMount(){
      window.scrollTo(0, 0);

      const api = 'https://api.atk-pelangimandiri.com/api/news/highlight';

      axios.get(api)
         .then(res => {

            this.setState({
               bunch_of_news: res.data,
            })

            setTimeout( () => {
               this.setState({loading: false});
            }, 1500);
         })
         .catch(err => {
            console.log(err)
         })
   }

   handleValue(e) {
      this.setState( {[e.target.name]: e.target.value} );
   }

   handleSubmit(e) {
      e.preventDefault();


      axios.post('https://api.atk-pelangimandiri.com/api/subscribe',{
         email: this.state.email
      })
      .then(response => {
         this.setState({
            email: '',
            isError: false,
            errorMessage: []
         })

         //Call React Alert
         this.showSuccessAlert();
      })
      .catch(error => {
         const status = error.response.status;

         if(status === 400)
         {   
            this.setState({
               isError: true,
               errorMessage: error.response.data.error_message
            })

            //Call React Alert
            this.showErrorAlert();
         }   
    
      })
   }

   //React Alert
   alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'dark',
      transition: 'scale'
   }

   showSuccessAlert = () => {
      this.msg.show('Terima kasih telah berlangganan berita dari kami!', {
         time: 10000,
         type: 'success',
      })
   }

   showErrorAlert = () => {
      this.msg.show('Harap mengisi email anda terlebih dahulu untuk berlangganan berita dari kami!', {
         time: 10000,
         type: 'error',
      })
   }

   getNewsHighlight() {

      const news_highlight = this.state.bunch_of_news.map((news) =>
         <div key={news.id} className="col-12 col-sm-12 col-md-6 news-box">
            <div className="card text-white bg-primary mb-3">
               <div className="card-header">
                  {news.created_at}
               </div>
               <div className="card-body">
                  <h4 className="card-title">{news.title}</h4>
                  <p className="card-text">{news.message.substring(0,50)+'...'}</p>
                  <Link className="btn btn-outline-secondary btn-lg news-button" to={"/news/" + news.id}>baca lebih lanjut<TiArrowRight /></Link>
              </div>
            </div>
         </div>
      );

      return news_highlight
   }

   render() {
      const { loading, email, errorMessage } = this.state

      if(loading)
      {
         return (
            <div className="loading-screen2">
               <Helmet>
                  <title>Berita</title>
               </Helmet>

               <div className="dizzy-gillespie"></div>
               <h1>loading...</h1>
            </div>
         );
      }

      return (
         <div className="container-fluid news-container">
            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12 news-title">
                  <h1>Berita</h1>
               </div>

               {this.getNewsHighlight()}
               
               <div className="col-12 col-sm-12 col-md-12">
                  <form className="subscribe-newsletter" onSubmit={this.handleSubmit}>
                     <h1>berlangganan berita dari kami</h1>
                     <input className="form-control" type="email" name="email" value={email} placeholder="Masukkan Email Anda" autoComplete="off" onChange={this.handleValue}/>
                     <span className="error-message">{errorMessage.email}</span>
                     <button type="submit" className="subscribe-button btn btn-primary btn-lg">berlangganan</button>
                     <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                  </form>
               </div>
            </div>
         </div>
      );
   }
}

export default News;