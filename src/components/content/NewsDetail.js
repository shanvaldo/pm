import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";

class NewsDetail extends Component {

   constructor(props) {
      super(props);
      this.state = {
         news_id : this.props.match.params.news_detail,
         news: '',
         loading: true
      };
   }

   componentWillMount(){
      window.scrollTo(0, 0);
      const {news_id} = this.state;
      const api = 'https://api.atk-pelangimandiri.com/api/news/'+news_id;

      axios.get(api)
         .then(res => {

            this.setState({
               news: res.data,
            })

            setTimeout( () => {
               this.setState({loading: false});
            }, 1500);
         })
         .catch(err => {
            console.log(err)
         })
   }

   render() {
      
      const { loading, news } = this.state

      if(loading)
      {
         return (
            <div className="loading-screen2">
               <Helmet>
                  <title>{"Berita - "+news.title}</title>
               </Helmet>

               <div className="dizzy-gillespie"></div>
               <h1>loading...</h1>
            </div>
         );
      }

      return (
         <div className="container-fluid news-detail-container">
            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="news-detail-title">
                     <h1>{news.title}</h1>
                     <hr className="custom-line"/>
                  </div>
                  <div className="news-detail-content">
                     <p>{news.message}</p>
                     <hr className="custom-line"/>
                     <span>tanggal:&nbsp;
                        <h2>{news.created_at}</h2>
                        <h2>&nbsp;|&nbsp;dipublikasikan oleh admin</h2>
                     </span>   
                  </div>
               </div>   
            </div>
         </div>
      );
   }
}

export default NewsDetail;