import React, { Component } from 'react';

import axios from 'axios';
import {Helmet} from "react-helmet";
import AlertContainer from 'react-alert';
import ReCAPTCHA from 'react-google-recaptcha';
import FaClockO from 'react-icons/lib/fa/clock-o';
import FaEnvelope from 'react-icons/lib/fa/envelope';
import FaFax from 'react-icons/lib/fa/fax';
import FaPhone from 'react-icons/lib/fa/phone';
import FaMapMarker from 'react-icons/lib/fa/map-marker';
import FaMap from 'react-icons/lib/fa/map';

class Contact extends Component {

   constructor(props) {
      super(props);
      this.state = {
         email: '',
         name: '',
         subject: '',
         message: '',
         isError: false,
         errorMessage: [],
         reCaptcha: false
      };

      this.handleValue = this.handleValue.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.reCaptcha = this.reCaptcha.bind(this);
   }

   componentWillMount(){
      window.scrollTo(0, 0);
   }

   handleValue(e) {
      this.setState( {[e.target.name]: e.target.value} );
   }

   handleSubmit(e) {
      e.preventDefault();

      if(this.state.reCaptcha === true)
      {

         axios.post('https://api.atk-pelangimandiri.com/api/support',{
            name: this.state.name,
            email: this.state.email,
            subject: this.state.subject,
            message: this.state.message
         })
         .then(response => {
            this.setState({
               email: '',
               name: '',
               subject: '',
               message: '',
               isError: false,
               errorMessage: []
            })
            
            //Call React Alert
            this.showAlert();
         })
         .catch(error => {
            const status = error.response.status;

            if(status === 400)
            {   
               this.setState({
                  isError: true,
                  errorMessage: error.response.data.error_message
               })
            }   
         })
      }
      else
      {
         this.reCaptchaAlert();
      }
   }

   //React Alert
   alertOptions = {
      offset: 14,
      position: 'top right',
      theme: 'dark',
      transition: 'scale'
   }

   showAlert = () => {
      this.msg.show('Terima kasih atas feedback anda!', {
         time: 10000,
         type: 'success',
      })
      window.grecaptcha.reset();
   }

   //Google ReCaptcha
   reCaptchaAlert = () => {
      this.msg.show('Harap memverifikasi ReCaptcha terlebih dahulu untuk menghubungi layanan support!', {
         time: 10000,
         type: 'error',
      })
   }

   reCaptcha(value) {
      // console.log("Captcha value:", value);
      this.setState({
         reCaptcha: true
      })
   }

   render() {

      const { name, email, subject, message, errorMessage } = this.state;

      return (
         <div className="container-fluid contact-container">
            <Helmet>
                <title>Hubungi Kami</title>
            </Helmet>

            <div className="row no-gutters">
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="contact-title">
                     <h1>Hubungi Kami</h1>
                  </div>
               </div>
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="contact-content">
                     <h1><FaClockO /> jam kerja&nbsp;:</h1>
                     <p className="working-hours">
                        <label>senin - jumat :</label>
                        <br/>
                        9:00&nbsp;WIB – 16:00&nbsp;WIB
                        <br/>
                     </p>
                     <hr className="custom-line"/>
                     <h1><FaMapMarker /> alamat :</h1>
                     <p className="address">
                        ITC mangga dua
                        <br/>
                        lt. dasar
                        <br/>
                        blok : e1, no : 50 - 51
                        <br/>
                        jln. mangga dua raya
                        <br/>
                        jakarta utara
                        <br/>
                        DKI jakarta
                        <br/>
                        <br/>
                        <FaEnvelope />&nbsp;<label id="email-label">email</label><span>:</span><span id="contact-email">atk.pelangimandiri@gmail.com</span>
                        <br/>
                        <FaPhone />&nbsp;<label>phone</label><span>:</span><a href="tel: 0216014555">021-6014555</a>
                        <br/>
                        <FaFax />&nbsp;<label>fax</label><span>:</span>021-6014554
                     </p>
                     <hr className="custom-line"/>
                  </div>
               </div>
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="google-map-container">
                     <h1><FaMap />&nbsp;temui kami sekarang</h1>
                     <iframe title="maps" width="100%" height="300" frameBorder="0"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyBR8JZE3k5CLqAMCocY_GvtJW8mc4zZw_E&q=place_id:ChIJoREXCAIeai4RcdhU-ZGBQKs"}>
                     </iframe>
                  </div>
               </div>
               <div className="col-12 col-sm-12 col-md-12">
                  <div className="contact-support">
                     <hr className="custom-line"/>
                     <h1>hubungi layanan support</h1>
                     
                     <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                           <label htmlFor="email_support">Email</label>
                           <input className="form-control" type="email" name="email" value={email} placeholder="Email anda" autoComplete="off" onChange={this.handleValue}/>
                           <span className="error-message">{errorMessage.email}</span>


                           <label htmlFor="name_support">Nama</label>
                           <input className="form-control" type="text" name="name" value={name} placeholder="Nama anda" autoComplete="off" onChange={this.handleValue}/>
                           <span className="error-message">{errorMessage.name}</span>

                           <label htmlFor="subject_support">Subjek</label>
                           <input className="form-control" type="text" name="subject" value={subject} placeholder="e.g 'Tanya harga'" autoComplete="off" onChange={this.handleValue}/>
                           <span className="error-message">{errorMessage.subject}</span>

                           <label htmlFor="message_support">Pesan anda</label>
                           <textarea className="form-control" name="message" value={message} rows="2" placeholder="Pesan saya untuk Pelangi Mandiri" onChange={this.handleValue}></textarea>
                           <span className="error-message">{errorMessage.message}</span>
                           <div className="captcha-container">
                              <ReCAPTCHA
                                 ref="recaptcha"
                                 sitekey="6LevzT0UAAAAAH2Qk-QR2ykPeIa2cSRkyjXf3wgw"
                                 onChange={this.reCaptcha}
                              />
                           </div>  
                           <button type="submit" className="btn btn-primary btn-lg">kirim</button>
                           <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
                       </div>
                     </form>  
                  </div>
               </div>
            </div>
         </div>
      );
   }
}

export default Contact;